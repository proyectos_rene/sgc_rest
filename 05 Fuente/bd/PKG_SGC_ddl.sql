CREATE OR REPLACE 
PACKAGE sgc.pkg_sgc
/* Formatted on 8-oct-2022 15:40:40 (QP5 v5.126) */
IS
    TYPE cursortype IS REF CURSOR;

    FUNCTION f_crear_persona (p_per_documento_nro      IN VARCHAR2,
                              p_per_documento_ext      IN VARCHAR2,
                              p_per_apellido_uno       IN VARCHAR2,
                              p_per_apellido_dos       IN VARCHAR2,
                              p_per_nombres            IN VARCHAR2,
                              p_per_fecha_nacimiento   IN VARCHAR2,
                              p_usuario                IN VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION f_crear_cuenta (p_per_documento_nro   IN VARCHAR2,
                             p_cue_numero          IN VARCHAR2,
                             p_tip_id              IN VARCHAR2,
                             p_usuario             IN VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION f_movimiento_cuenta (p_per_documento_nro   IN VARCHAR2,
                                  p_cue_numero          IN VARCHAR2,
                                  p_mov_monto           IN NUMBER,
                                  p_mov_tipo            IN VARCHAR2,
                                  p_usuario             IN VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION f_consulta_cuenta (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype;

    FUNCTION f_consulta_persona (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype;

    FUNCTION f_total_ingreso_salida (p_per_documento_nro   IN VARCHAR2,
                                     p_cue_numero          IN VARCHAR2,
                                     p_accion                 VARCHAR2)
        RETURN NUMBER;

    FUNCTION f_saldo (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype;

    --------------------------------------------------------------------------------
    -- VALIDACION
    --------------------------------------------------------------------------------

    FUNCTION f_validar_persona (p_per_documento_nro IN VARCHAR2)
        RETURN NUMBER;

    FUNCTION f_validar_tipo_cuenta (p_tip_id IN VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION f_validar_nro_cuenta (p_per_documento_nro   IN VARCHAR2,
                                   p_cue_numero          IN VARCHAR2)
        RETURN VARCHAR2;

    FUNCTION f_correlativo
        RETURN NUMBER;
END pkg_sgc;
/

CREATE OR REPLACE 
PACKAGE BODY sgc.pkg_sgc
/* Formatted on 08/10/2022 16:54:26 (QP5 v5.126) */
IS
    --
    -- f_crear_persona
    --
    FUNCTION f_crear_persona (p_per_documento_nro      IN VARCHAR2,
                              p_per_documento_ext      IN VARCHAR2,
                              p_per_apellido_uno       IN VARCHAR2,
                              p_per_apellido_dos       IN VARCHAR2,
                              p_per_nombres            IN VARCHAR2,
                              p_per_fecha_nacimiento   IN VARCHAR2,
                              p_usuario                IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_result    VARCHAR2 (255) := '';
        v_cont      NUMBER;
        v_version   NUMBER;
    BEGIN
        IF f_validar_persona (p_per_documento_nro) > 0
        THEN
            RETURN    'Persona con el documento '
                   || p_per_documento_nro
                   || ' ya se encuentra registrado.';
        END IF;

        INSERT INTO sgc_persona (per_documento_nro,
                                 per_documento_ext,
                                 per_apellido_uno,
                                 per_apellido_dos,
                                 per_nombres,
                                 per_fecha_nacimiento,
                                 usu_mod,
                                 fec_mod,
                                 lst_ope,
                                 num_ver)
          VALUES   (p_per_documento_nro,
                    p_per_documento_ext,
                    p_per_apellido_uno,
                    p_per_apellido_dos,
                    p_per_nombres,
                    TO_DATE (p_per_fecha_nacimiento, 'dd/mm/yyyy'),
                    p_usuario,
                    SYSDATE,
                    'U',
                    0);

        COMMIT;
        RETURN 'OK';
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            v_result := SUBSTR (TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255);
            RETURN v_result;
    END f_crear_persona;

    --
    -- f_crear_cuenta
    --
    FUNCTION f_crear_cuenta (p_per_documento_nro   IN VARCHAR2,
                             p_cue_numero          IN VARCHAR2,
                             p_tip_id              IN VARCHAR2,
                             p_usuario             IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_result    VARCHAR2 (255) := '';
        v_cont      NUMBER;
        v_version   NUMBER;
    BEGIN
        IF f_validar_persona (p_per_documento_nro) = 0
        THEN
            RETURN    'Antes de crear el nro de cuenta '
                   || p_cue_numero
                   || ' debe registrar previamente a la persona.';
        END IF;

        v_result := f_validar_tipo_cuenta (p_tip_id);

        IF v_result != 'OK'
        THEN
            RETURN v_result;
        END IF;

        SELECT   COUNT (1)
          INTO   v_cont
          FROM   sgc_cuenta
         WHERE       lst_ope = 'U'
                 AND num_ver = 0
                 AND per_documento_nro = p_per_documento_nro
                 AND cue_numero = p_cue_numero;

        IF v_cont > 0
        THEN
            RETURN    'EL nro de cuenta '
                   || p_cue_numero
                   || ' asociado al documento '
                   || p_per_documento_nro
                   || ' ya existe.';
        END IF;

        INSERT INTO sgc_cuenta (per_documento_nro,
                                cue_numero,
                                tip_id,
                                cue_estado,
                                usu_mod,
                                fec_mod,
                                lst_ope,
                                num_ver)
          VALUES   (p_per_documento_nro,
                    p_cue_numero,
                    p_tip_id,
                    'ACTIVE',
                    p_usuario,
                    SYSDATE,
                    'U',
                    0);

        COMMIT;
        RETURN 'OK';
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            v_result := SUBSTR (TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255);
            RETURN v_result;
    END f_crear_cuenta;

    --
    -- f_movimiento_cuenta
    --
    FUNCTION f_movimiento_cuenta (p_per_documento_nro   IN VARCHAR2,
                                  p_cue_numero          IN VARCHAR2,
                                  p_mov_monto           IN NUMBER,
                                  p_mov_tipo            IN VARCHAR2,
                                  p_usuario             IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_result       VARCHAR2 (255) := '';
        v_cont         NUMBER;
        v_version      NUMBER;
        v_secuencial   NUMBER;
    BEGIN
        IF f_validar_persona (p_per_documento_nro) = 0
        THEN
            RETURN    'Persona con el Nro. de documento '
                   || p_per_documento_nro
                   || ' no existe.';
        END IF;

        v_result := f_validar_nro_cuenta (p_per_documento_nro, p_cue_numero);

        IF p_mov_monto <= 0
        THEN
            RETURN 'El monto debe ser mayor a cero.';
        END IF;

        IF v_result != 'OK'
        THEN
            RETURN v_result;
        END IF;

        IF p_mov_tipo = 'SALIDA'
        THEN
            SELECT   COUNT (1)
              INTO   v_cont
              FROM   sgc_cuenta
             WHERE       lst_ope = 'U'
                     AND num_ver = 0
                     AND per_documento_nro = p_per_documento_nro
                     AND cue_numero = p_cue_numero
                     AND cue_estado = 'HOLD';

            IF v_cont = 0
            THEN
                SELECT   COUNT (1)
                  INTO   v_cont
                  FROM   sgc_cuenta
                 WHERE       lst_ope = 'U'
                         AND num_ver = 0
                         AND per_documento_nro = p_per_documento_nro
                         AND cue_numero = p_cue_numero
                         AND cue_estado = 'ACTIVE';

                -- CONSULTAMOS EL SALDO
                IF (f_total_ingreso_salida (p_per_documento_nro,
                                            p_cue_numero,
                                            'INGRESO')
                    - f_total_ingreso_salida (p_per_documento_nro,
                                              p_cue_numero,
                                              'SALIDA')) < p_mov_monto
                   AND v_cont > 0
                THEN
                    -- versionar cabecera
                    SELECT   NVL (MAX (num_ver) + 1, 1)
                      INTO   v_version
                      FROM   sgc_cuenta
                     WHERE   per_documento_nro = p_per_documento_nro
                             AND cue_numero = p_cue_numero;

                    -- actualizamo la version cero cabecera
                    UPDATE   sgc_cuenta
                       SET   num_ver = v_version
                     WHERE       per_documento_nro = p_per_documento_nro
                             AND cue_numero = p_cue_numero
                             AND num_ver = 0;

                    INSERT INTO sgc_cuenta
                        SELECT   per_documento_nro,
                                 cue_numero,
                                 tip_id,
                                 'HOLD',
                                 p_usuario,
                                 SYSDATE,
                                 'U',
                                 0
                          FROM   sgc_cuenta
                         WHERE       per_documento_nro = p_per_documento_nro
                                 AND cue_numero = p_cue_numero
                                 AND lst_ope = 'U'
                                 AND num_ver = v_version;
                END IF;
            ELSE
                RETURN    'No puede realizar retiros de su cuenta '
                       || p_cue_numero
                       || ' en estado HOLD.';
            END IF;
        ELSIF p_mov_tipo = 'INGRESO'
        THEN
            SELECT   COUNT (1)
              INTO   v_cont
              FROM   sgc_cuenta
             WHERE       lst_ope = 'U'
                     AND num_ver = 0
                     AND per_documento_nro = p_per_documento_nro
                     AND cue_numero = p_cue_numero
                     AND cue_estado = 'HOLD';

            -- CONSULTAMOS EL SALDO
            IF p_mov_monto >
                   (f_total_ingreso_salida (p_per_documento_nro,
                                            p_cue_numero,
                                            'INGRESO')
                    - f_total_ingreso_salida (p_per_documento_nro,
                                              p_cue_numero,
                                              'SALIDA'))
               AND v_cont > 0
            THEN
                -- versionar cabecera
                SELECT   NVL (MAX (num_ver) + 1, 1)
                  INTO   v_version
                  FROM   sgc_cuenta
                 WHERE   per_documento_nro = p_per_documento_nro
                         AND cue_numero = p_cue_numero;

                -- actualizamo la version cero cabecera
                UPDATE   sgc_cuenta
                   SET   num_ver = v_version
                 WHERE       per_documento_nro = p_per_documento_nro
                         AND cue_numero = p_cue_numero
                         AND num_ver = 0;

                INSERT INTO sgc_cuenta
                    SELECT   per_documento_nro,
                             cue_numero,
                             tip_id,
                             'ACTIVE',
                             p_usuario,
                             SYSDATE,
                             'U',
                             0
                      FROM   sgc_cuenta
                     WHERE       per_documento_nro = p_per_documento_nro
                             AND cue_numero = p_cue_numero
                             AND lst_ope = 'U'
                             AND num_ver = v_version;
            END IF;
        ELSE
            RETURN 'Error de movimiento, no corresponde a INGRESO o SALIDA';
        END IF;

        v_secuencial := TO_CHAR (SYSDATE, 'YYYY') || pkg_sgc.f_correlativo;

        INSERT INTO sgc_movimiento (mov_id,
                                    per_documento_nro,
                                    cue_numero,
                                    mov_monto,
                                    mov_tipo,
                                    usu_mod,
                                    fec_mod,
                                    lst_ope,
                                    num_ver)
          VALUES   (v_secuencial,
                    p_per_documento_nro,
                    p_cue_numero,
                    p_mov_monto,
                    p_mov_tipo,
                    p_usuario,
                    SYSDATE,
                    'U',
                    0);

        COMMIT;
        RETURN 'OK';
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            v_result := SUBSTR (TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255);
            RETURN v_result;
    END f_movimiento_cuenta;

    --
    -- f_consulta_cuenta
    --

    FUNCTION f_consulta_cuenta (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype
    IS
        p_salida   cursortype;
    BEGIN
        OPEN p_salida FOR
              SELECT   m.mov_id,
                       c.per_documento_nro,
                       c.cue_numero,
                       tc.tip_nombre,
                       tc.tip_moneda,
                       m.mov_monto,
                       m.mov_tipo,
                       c.cue_estado,
                       m.usu_mod,
                       m.fec_mod
                FROM   sgc_movimiento m, sgc_cuenta c, sgc_tipo_cuenta tc
               WHERE       m.per_documento_nro(+) = c.per_documento_nro
                       AND m.cue_numero(+) = c.cue_numero
                       AND m.lst_ope(+) = c.lst_ope
                       AND m.num_ver(+) = c.num_ver
                       AND m.lst_ope(+) = 'U'
                       AND m.num_ver(+) = 0
                       AND tc.tip_id = c.tip_id
                       AND tc.lst_ope = c.lst_ope
                       AND tc.num_ver = c.num_ver
                       AND c.per_documento_nro = p_per_documento_nro
            ORDER BY   c.cue_numero, m.fec_mod ASC;

        RETURN p_salida;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;

            OPEN p_salida FOR
                SELECT   *
                  FROM   DUAL
                 WHERE   1 = 2;

            RETURN p_salida;
    END f_consulta_cuenta;

    --
    -- f_consulta_persona
    --

    FUNCTION f_consulta_persona (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype
    IS
        p_salida   cursortype;
    BEGIN
        OPEN p_salida FOR
            SELECT   per_documento_nro,
                     per_documento_ext,
                     per_apellido_uno,
                     per_apellido_dos,
                     per_nombres,
                     TO_CHAR (per_fecha_nacimiento, 'dd/mm/yyyy')
              FROM   sgc_persona
             WHERE       lst_ope = 'U'
                     AND num_ver = 0
                     AND per_documento_nro = p_per_documento_nro;

        RETURN p_salida;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;

            OPEN p_salida FOR
                SELECT   *
                  FROM   DUAL
                 WHERE   1 = 2;

            RETURN p_salida;
    END f_consulta_persona;

    FUNCTION f_total_ingreso_salida (p_per_documento_nro   IN VARCHAR2,
                                     p_cue_numero          IN VARCHAR2,
                                     p_accion                 VARCHAR2)
        RETURN NUMBER
    IS
        v_total   NUMBER := 0;
    BEGIN
        SELECT   NVL (SUM (mov_monto), 0)
          INTO   v_total
          FROM   sgc_movimiento
         WHERE       per_documento_nro = p_per_documento_nro
                 AND cue_numero = p_cue_numero
                 AND lst_ope = 'U'
                 AND num_ver = 0
                 AND mov_tipo = p_accion;

        RETURN v_total;
    END f_total_ingreso_salida;

    FUNCTION f_saldo (p_per_documento_nro IN VARCHAR2)
        RETURN cursortype
    IS
        p_salida          cursortype;
        v_total_ingreso   NUMBER := 0;
        v_total_salida    NUMBER := 0;
    BEGIN
        OPEN p_salida FOR
            SELECT   c.cue_numero,
                     (SELECT   NVL (SUM (mov_monto), 0)
                        FROM   sgc_movimiento m1
                       WHERE       m1.per_documento_nro = c.per_documento_nro
                               AND m1.cue_numero = c.cue_numero
                               AND m1.lst_ope = 'U'
                               AND m1.num_ver = 0
                               AND m1.mov_tipo = 'INGRESO')
                     - (SELECT   NVL (SUM (mov_monto), 0)
                          FROM   sgc_movimiento m2
                         WHERE   m2.per_documento_nro = c.per_documento_nro
                                 AND m2.cue_numero = c.cue_numero
                                 AND m2.lst_ope = 'U'
                                 AND m2.num_ver = 0
                                 AND m2.mov_tipo = 'SALIDA')
                         saldo
              FROM   sgc_cuenta c
             WHERE       c.lst_ope = 'U'
                     AND c.num_ver = 0
                     AND c.per_documento_nro = p_per_documento_nro;

        RETURN p_salida;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;

            OPEN p_salida FOR
                SELECT   *
                  FROM   DUAL
                 WHERE   1 = 2;
    END f_saldo;

    --------------------------------------------------------------------------------
    -- VALIDACION
    --------------------------------------------------------------------------------
    -- f_validar_persona

    FUNCTION f_validar_persona (p_per_documento_nro IN VARCHAR2)
        RETURN NUMBER
    IS
        v_cont   NUMBER;
    BEGIN
        SELECT   COUNT (1)
          INTO   v_cont
          FROM   sgc_persona
         WHERE       per_documento_nro = p_per_documento_nro
                 AND lst_ope = 'U'
                 AND num_ver = 0;

        RETURN v_cont;
    END f_validar_persona;

    --
    -- f_validar_tipo_cuenta
    --

    FUNCTION f_validar_tipo_cuenta (p_tip_id IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_result   VARCHAR2 (255) := 'OK';
        v_cont     NUMBER;
    BEGIN
        SELECT   COUNT (1)
          INTO   v_cont
          FROM   sgc_tipo_cuenta
         WHERE   tip_id = p_tip_id AND lst_ope = 'U' AND num_ver = 0;

        IF v_cont = 0
        THEN
            v_result := 'Tipo de cuenta ' || p_tip_id || ' no existe.';
        END IF;

        RETURN v_result;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            v_result := SUBSTR (TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255);
            RETURN v_result;
    END f_validar_tipo_cuenta;

    --
    -- f_validar_nro_cuenta
    --

    FUNCTION f_validar_nro_cuenta (p_per_documento_nro   IN VARCHAR2,
                                   p_cue_numero          IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_result   VARCHAR2 (255) := 'OK';
        v_cont     NUMBER;
    BEGIN
        SELECT   COUNT (1)
          INTO   v_cont
          FROM   sgc_cuenta
         WHERE       per_documento_nro = p_per_documento_nro
                 AND cue_numero = p_cue_numero
                 AND lst_ope = 'U'
                 AND num_ver = 0;

        IF v_cont = 0
        THEN
            v_result :=
                   'Nro. de Cuenta  '
                || p_cue_numero
                || ' no existe para el documento '
                || p_per_documento_nro;
        END IF;

        RETURN v_result;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            v_result := SUBSTR (TO_CHAR (SQLCODE) || ': ' || SQLERRM, 1, 255);
            RETURN v_result;
    END f_validar_nro_cuenta;

    --
    -- f_correlativo
    --

    FUNCTION f_correlativo
        RETURN NUMBER
    IS
        v_correlativo   NUMBER;
        v_cont          NUMBER;
    BEGIN
        SELECT   COUNT (1)
          INTO   v_cont
          FROM   sgc_correlativo
         WHERE   cor_gestion = TO_CHAR (SYSDATE, 'YYYY');

        IF v_cont > 0
        THEN
            SELECT   MAX (cor_sec) + 1
              INTO   v_correlativo
              FROM   sgc_correlativo
             WHERE   cor_gestion = TO_CHAR (SYSDATE, 'YYYY');

            UPDATE   sgc_correlativo
               SET   cor_sec = v_correlativo
             WHERE   cor_gestion = TO_CHAR (SYSDATE, 'YYYY');
        ELSE
            v_correlativo := 1;

            INSERT INTO sgc_correlativo (cor_gestion, cor_sec)
              VALUES   (TO_CHAR (SYSDATE, 'YYYY'), v_correlativo);
        END IF;

        RETURN v_correlativo;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            RETURN 0;
    END f_correlativo;
END;
/

