CREATE TABLE sgc.sgc_correlativo
    (cor_gestion                    NUMBER(4,0) NOT NULL,
    cor_sec                        NUMBER(10,0) )
  NOPARALLEL
  LOGGING
/

ALTER TABLE sgc.sgc_correlativo
ADD CONSTRAINT pk_sgc_correlativo PRIMARY KEY (cor_gestion, cor_sec)
USING INDEX
/

COMMENT ON COLUMN sgc.sgc_correlativo.cor_gestion IS 'GESTION'
/
COMMENT ON COLUMN sgc.sgc_correlativo.cor_sec IS 'CORRELATIVO'
/
CREATE TABLE sgc.sgc_cuenta
    (per_documento_nro              VARCHAR2(15 BYTE) NOT NULL,
    cue_numero                     VARCHAR2(55 BYTE) NOT NULL,
    tip_id                         VARCHAR2(15 BYTE) NOT NULL,
    cue_estado                     VARCHAR2(15 BYTE) NOT NULL,
    usu_mod                        VARCHAR2(35 BYTE) NOT NULL,
    fec_mod                        DATE NOT NULL,
    lst_ope                        VARCHAR2(1 BYTE) NOT NULL,
    num_ver                        NUMBER(10,0) NOT NULL)
  NOPARALLEL
  LOGGING
/

COMMENT ON COLUMN sgc.sgc_cuenta.cue_estado IS 'ESTADO DE LA CUENTA: ACTIVE/HOLD'
/
COMMENT ON COLUMN sgc.sgc_cuenta.cue_numero IS 'NUMERO DE CUENTA'
/
COMMENT ON COLUMN sgc.sgc_cuenta.fec_mod IS 'DATOS DE AUDITORIA: FECHA DE REGISTRO'
/
COMMENT ON COLUMN sgc.sgc_cuenta.lst_ope IS 'DATOS DE AUDITORIA: ESTADO DE REGISTRO, U: ACTIVO D:INACTIVO'
/
COMMENT ON COLUMN sgc.sgc_cuenta.num_ver IS 'DATOS DE AUDITORIA: VERSIONAMIENTO'
/
COMMENT ON COLUMN sgc.sgc_cuenta.per_documento_nro IS 'NRO DOCUMENTO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_cuenta.tip_id IS 'ID TIPO DE CUENTA'
/
COMMENT ON COLUMN sgc.sgc_cuenta.usu_mod IS 'DATOS DE AUDITORIA: USUARIO DE REGISTRO'
/
CREATE TABLE sgc.sgc_movimiento
    (mov_id                         NUMBER NOT NULL,
    per_documento_nro              VARCHAR2(15 BYTE),
    cue_numero                     VARCHAR2(55 BYTE),
    mov_monto                      NUMBER NOT NULL,
    mov_tipo                       VARCHAR2(55 BYTE) NOT NULL,
    usu_mod                        VARCHAR2(35 BYTE) NOT NULL,
    fec_mod                        DATE NOT NULL,
    lst_ope                        VARCHAR2(1 BYTE) NOT NULL,
    num_ver                        NUMBER(10,0) NOT NULL)
  NOPARALLEL
  LOGGING
/

ALTER TABLE sgc.sgc_movimiento
ADD CONSTRAINT pk_sgc_movimiento PRIMARY KEY (mov_id, num_ver)
USING INDEX
/

COMMENT ON COLUMN sgc.sgc_movimiento.cue_numero IS 'NUMERO DE CUENTA'
/
COMMENT ON COLUMN sgc.sgc_movimiento.fec_mod IS 'DATOS DE AUDITORIA: FECHA DE REGISTRO'
/
COMMENT ON COLUMN sgc.sgc_movimiento.lst_ope IS 'DATOS DE AUDITORIA: ESTADO DE REGISTRO, U: ACTIVO D:INACTIVO'
/
COMMENT ON COLUMN sgc.sgc_movimiento.mov_id IS 'ID TRANSACCION'
/
COMMENT ON COLUMN sgc.sgc_movimiento.mov_monto IS 'MONTO MOVIEMIENTO'
/
COMMENT ON COLUMN sgc.sgc_movimiento.mov_tipo IS 'TIPO DEL MOVIMIENTO: INGRESO/SALIDA'
/
COMMENT ON COLUMN sgc.sgc_movimiento.num_ver IS 'DATOS DE AUDITORIA: VERSIONAMIENTO'
/
COMMENT ON COLUMN sgc.sgc_movimiento.per_documento_nro IS 'NRO DOCUMENTO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_movimiento.usu_mod IS 'DATOS DE AUDITORIA: USUARIO DE REGISTRO'
/
CREATE TABLE sgc.sgc_persona
    (per_documento_nro              VARCHAR2(15 BYTE) NOT NULL,
    per_documento_ext              VARCHAR2(15 BYTE),
    per_apellido_uno               VARCHAR2(55 BYTE),
    per_apellido_dos               VARCHAR2(55 BYTE),
    per_nombres                    VARCHAR2(55 BYTE) NOT NULL,
    per_fecha_nacimiento           DATE NOT NULL,
    usu_mod                        VARCHAR2(35 BYTE) NOT NULL,
    fec_mod                        DATE NOT NULL,
    lst_ope                        VARCHAR2(1 BYTE) NOT NULL,
    num_ver                        NUMBER(10,0) NOT NULL)
  NOPARALLEL
  LOGGING
/

ALTER TABLE sgc.sgc_persona
ADD CONSTRAINT pk_sgc_persona PRIMARY KEY (per_documento_nro, num_ver)
USING INDEX
/

COMMENT ON COLUMN sgc.sgc_persona.fec_mod IS 'DATOS DE AUDITORIA: FECHA DE REGISTRO'
/
COMMENT ON COLUMN sgc.sgc_persona.lst_ope IS 'DATOS DE AUDITORIA: ESTADO DE REGISTRO, U: ACTIVO D:INACTIVO'
/
COMMENT ON COLUMN sgc.sgc_persona.num_ver IS 'DATOS DE AUDITORIA: VERSIONAMIENTO'
/
COMMENT ON COLUMN sgc.sgc_persona.per_apellido_dos IS 'SEGUNDO APELLIDO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_persona.per_apellido_uno IS 'PRIMER APELLIDO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_persona.per_documento_ext IS 'EXTENSION DEL NRO DE DOCUMENTO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_persona.per_documento_nro IS 'NRO DOCUMENTO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_persona.per_nombres IS 'NOMBRE COMPLETO DE LA PERSONA'
/
COMMENT ON COLUMN sgc.sgc_persona.usu_mod IS 'DATOS DE AUDITORIA: USUARIO DE REGISTRO'
/
CREATE TABLE sgc.sgc_tipo_cuenta
    (tip_id                         VARCHAR2(15 BYTE) NOT NULL,
    tip_nombre                     VARCHAR2(55 BYTE) NOT NULL,
    tip_moneda                     VARCHAR2(15 BYTE) NOT NULL,
    usu_mod                        VARCHAR2(35 BYTE) NOT NULL,
    fec_mod                        DATE NOT NULL,
    lst_ope                        VARCHAR2(1 BYTE) NOT NULL,
    num_ver                        NUMBER(10,0) NOT NULL)
  NOPARALLEL
  LOGGING
/

ALTER TABLE sgc.sgc_tipo_cuenta
ADD CONSTRAINT pk_sgc_tipo_cuenta PRIMARY KEY (tip_id, num_ver)
USING INDEX
/

COMMENT ON COLUMN sgc.sgc_tipo_cuenta.fec_mod IS 'DATOS DE AUDITORIA: FECHA DE REGISTRO'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.lst_ope IS 'DATOS DE AUDITORIA: ESTADO DE REGISTRO, U: ACTIVO D:INACTIVO'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.num_ver IS 'DATOS DE AUDITORIA: VERSIONAMIENTO'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.tip_id IS 'ID TIPO DE CUENTA'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.tip_moneda IS 'TIPO DE MONEDA, BOB: BOLIVIANO, USD:DOLARES'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.tip_nombre IS 'DESCRIPCION DEL TIPO DE CUENTA'
/
COMMENT ON COLUMN sgc.sgc_tipo_cuenta.usu_mod IS 'DATOS DE AUDITORIA: USUARIO DE REGISTRO'
/
