package sgc;

import java.sql.CallableStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import sgc.bean.Consulta;
import sgc.bean.Cuenta;
import sgc.bean.Movimiento;
import sgc.bean.Persona;

import sgc.bean.Resultado;

import sgc.bean.Saldo;

import sgc.general.Conexion;

public class Consultas {

    public Resultado savePersona(Persona bean, String p_usuario, Conexion con) {
        Resultado res = new Resultado();
        try {
            CallableStatement call = null;
            call = con.getCn().prepareCall("{? = call pkg_sgc.f_crear_persona( ?,?,?,  ?,?,?,  ? )}");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setString(2, bean.getPerDocumentoNro());
            call.setString(3, bean.getPerDocumentoExt());
            call.setString(4, bean.getPerApellidoUno());
            call.setString(5, bean.getPerApellidoDos());
            call.setString(6, bean.getPerNombres());
            call.setString(7, bean.getPerFechaNacimiento());
            call.setString(8, p_usuario);

            call.execute();
            String vSalida = call.getString(1);
            res.setCodigo("0");
            res.setMensaje(vSalida);
        } catch (Exception e) {
            res.setCodigo("1");
            res.setMensaje("ERROR savePersona: " + e.getMessage());
        }
        return res;
    }

    public Resultado saveCuenta(Cuenta bean, String p_usuario, Conexion con) {
        Resultado res = new Resultado();
        try {
            CallableStatement call = null;
            call = con.getCn().prepareCall("{? = call pkg_sgc.f_crear_cuenta( ?,?,?,  ?)}");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setString(2, bean.getPersona().getPerDocumentoNro());
            call.setString(3, bean.getCueNumero());
            call.setString(4, bean.getTipoCuenta().getTipId());
            call.setString(5, p_usuario);

            call.execute();
            String vSalida = call.getString(1);
            res.setCodigo("0");
            res.setMensaje(vSalida);
        } catch (Exception e) {
            res.setCodigo("1");
            res.setMensaje("ERROR saveCuenta: " + e.getMessage());
        }
        return res;
    }

    public Resultado saveMovimiento(Movimiento bean, String p_usuario, Conexion con) {
        Resultado res = new Resultado();
        try {
            CallableStatement call = null;
            call = con.getCn().prepareCall("{? = call pkg_sgc.f_movimiento_cuenta( ?,?,?,  ?,?)}");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setString(2, bean.getPerDocumentoNro());
            call.setString(3, bean.getCueNumero());
            call.setDouble(4, bean.getMovMonto());
            call.setString(5, bean.getMovTipo());
            call.setString(6, p_usuario);

            call.execute();
            String vSalida = call.getString(1);
            res.setCodigo("0");
            res.setMensaje(vSalida);
        } catch (Exception e) {
            res.setCodigo("1");
            res.setMensaje("ERROR saveMovimiento: " + e.getMessage());
        }
        return res;
    }

    public Consulta listaMovimiento(String p_documento, Conexion con) {
        Consulta res = new Consulta();
        try {
            CallableStatement call = null;
            call = con.getCn().prepareCall("{? = call pkg_sgc.f_consulta_persona(?)}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, p_documento);

            call.execute();
            ResultSet rs = (ResultSet)call.getObject(1);
            if (!(rs == null || !rs.next())) {
                res.getPersona().setPerDocumentoNro(rs.getString(1));
                res.getPersona().setPerDocumentoExt(rs.getString(2));
                res.getPersona().setPerApellidoUno(rs.getString(3));
                res.getPersona().setPerApellidoDos(rs.getString(4));
                res.getPersona().setPerNombres(rs.getString(5));
                res.getPersona().setPerFechaNacimiento(rs.getString(6));

                call = null;
                call = con.getCn().prepareCall("{? = call pkg_sgc.f_consulta_cuenta(?)}");
                call.registerOutParameter(1, OracleTypes.CURSOR);
                call.setString(2, p_documento);

                call.execute();
                ResultSet rs02 = (ResultSet)call.getObject(1);
                List<Movimiento> movimientos = null;
                if (!(rs02 == null || !rs02.next())) {
                    movimientos = new ArrayList<Movimiento>();
                    do {
                        Movimiento bean = new Movimiento();
                        bean.setMovId(rs02.getString(1));
                        bean.setPerDocumentoNro(rs02.getString(2));
                        bean.setCueNumero(rs02.getString(3));
                        bean.setTipNombre(rs02.getString(4));
                        bean.setTipMoneda(rs02.getString(5));
                        bean.setMovMonto(rs02.getDouble(6));
                        bean.setMovTipo(rs02.getString(7));
                        bean.setCueEstado(rs02.getString(8));
                        bean.setMovUsuario(rs02.getString(9));
                        bean.setMovFecha(rs02.getString(10));
                        movimientos.add(bean);
                    } while (rs02.next());
                    res.setMovimiento(movimientos);
                }

                call = null;
                call = con.getCn().prepareCall("{? = call pkg_sgc.f_saldo(?)}");
                call.registerOutParameter(1, OracleTypes.CURSOR);
                call.setString(2, p_documento);

                call.execute();
                ResultSet rs03 = (ResultSet)call.getObject(1);
                List<Saldo> saldos = null;
                if (!(rs03 == null || !rs03.next())) {
                    saldos = new ArrayList<Saldo>();
                    do {
                        Saldo bean = new Saldo();
                        bean.setCueNumero(rs03.getString(1));
                        bean.setSaldo(rs03.getDouble(2));
                        saldos.add(bean);
                    } while (rs03.next());
                    res.setSaldo(saldos);
                }
            }
        } catch (SQLException e1) {
            System.out.println("error listaMovimiento:" + e1.getMessage());
        } catch (Exception e2) {
            System.out.println("error listaMovimiento:" + e2.getMessage());
        }
        return res;
    }
}
