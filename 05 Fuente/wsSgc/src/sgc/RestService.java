package sgc;

import com.sun.jersey.multipart.FormDataParam;

import java.io.UnsupportedEncodingException;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.xml.bind.JAXB;

import sgc.bean.Consulta;
import sgc.bean.Cuenta;
import sgc.bean.Movimiento;
import sgc.bean.Persona;
import sgc.bean.Respuesta;

import sgc.bean.Resultado;

import sgc.general.Conexion;

@Path("rest/sgc")
public class RestService {
    private String fechab = "";

    @GET
    @Path("dummy/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response Dummy(@PathParam("id")
        String id) {
        Resultado res = new Resultado();
        res.setCodigo("0");
        res.setMensaje("El servicio funciona correctamente: " + id);
        return Response.status(200).entity(res).build();
    }

    @POST
    @Path("persona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response savePersona(Persona persona) {
        Resultado res = new Resultado();
        Consultas consulta = new Consultas();
        Conexion con = new Conexion();
        try {
            // Abrir coneccion
            con.abreConexion();
            // Persistir Persona
            Resultado resultado = new Resultado();
            resultado = consulta.savePersona(persona, "PRUEBA", con);
            if ("OK".equals(resultado.getMensaje())) {
                con.getCn().commit();
                res.setCodigo("0");
                res.setMensaje("OK");
            } else {
                con.getCn().rollback();
                res.setCodigo(resultado.getCodigo());
                res.setMensaje(resultado.getMensaje());
            }

            if (res.getCodigo().equals("0")) {
                return Response.status(200).entity(res).build();
            } else if (res.getCodigo().equals("1")) {
                return Response.status(401).entity(res).build();
            } else {
                return Response.status(409).entity(res).build();
            }
        } catch (UnsupportedEncodingException e1) {
            res.setCodigo("1");
            res.setMensaje(e1.getMessage());
            return Response.status(401).entity(res).build();
        } catch (Exception e2) {
            res.setCodigo("1");
            res.setMensaje(e2.getMessage());
            return Response.status(401).entity(res).build();
        } finally {
            con.cierraConexion();
        }
    }

    @POST
    @Path("cuenta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveCuenta(Cuenta cuenta) {
        Resultado res = new Resultado();
        Consultas consulta = new Consultas();
        Conexion con = new Conexion();
        try {
            // Abrir coneccion
            con.abreConexion();
            // Persistir Persona
            Resultado resultado = new Resultado();
            resultado = consulta.saveCuenta(cuenta, "PRUEBA", con);
            if ("OK".equals(resultado.getMensaje())) {
                con.getCn().commit();
                res.setCodigo("0");
                res.setMensaje("OK");
            } else {
                con.getCn().rollback();
                res.setCodigo(resultado.getCodigo());
                res.setMensaje(resultado.getMensaje());
            }

            if (res.getCodigo().equals("0")) {
                return Response.status(200).entity(res).build();
            } else if (res.getCodigo().equals("1")) {
                return Response.status(401).entity(res).build();
            } else {
                return Response.status(409).entity(res).build();
            }
        } catch (UnsupportedEncodingException e1) {
            res.setCodigo("1");
            res.setMensaje(e1.getMessage());
            return Response.status(401).entity(res).build();
        } catch (Exception e2) {
            res.setCodigo("1");
            res.setMensaje(e2.getMessage());
            return Response.status(401).entity(res).build();
        } finally {
            con.cierraConexion();
        }
    }

    @POST
    @Path("ingreso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveIngreso(Movimiento movimiento) {
        Resultado res = new Resultado();
        Consultas consulta = new Consultas();
        Conexion con = new Conexion();
        try {
            // Abrir coneccion
            con.abreConexion();
            // Persistir Persona
            Resultado resultado = new Resultado();
            movimiento.setMovTipo("INGRESO");
            resultado = consulta.saveMovimiento(movimiento, "PRUEBA", con);
            if ("OK".equals(resultado.getMensaje())) {
                con.getCn().commit();
                res.setCodigo("0");
                res.setMensaje("OK");
            } else {
                con.getCn().rollback();
                res.setCodigo(resultado.getCodigo());
                res.setMensaje(resultado.getMensaje());
            }

            if (res.getCodigo().equals("0")) {
                return Response.status(200).entity(res).build();
            } else if (res.getCodigo().equals("1")) {
                return Response.status(401).entity(res).build();
            } else {
                return Response.status(409).entity(res).build();
            }
        } catch (UnsupportedEncodingException e1) {
            res.setCodigo("1");
            res.setMensaje(e1.getMessage());
            return Response.status(401).entity(res).build();
        } catch (Exception e2) {
            res.setCodigo("1");
            res.setMensaje(e2.getMessage());
            return Response.status(401).entity(res).build();
        } finally {
            con.cierraConexion();
        }
    }

    @POST
    @Path("retiro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveRetiro(Movimiento movimiento) {
        Resultado res = new Resultado();
        Consultas consulta = new Consultas();
        Conexion con = new Conexion();
        try {
            // Abrir coneccion
            con.abreConexion();
            // Persistir Persona
            Resultado resultado = new Resultado();
            movimiento.setMovTipo("SALIDA");
            resultado = consulta.saveMovimiento(movimiento, "PRUEBA", con);
            if ("OK".equals(resultado.getMensaje())) {
                con.getCn().commit();
                res.setCodigo("0");
                res.setMensaje("OK");
            } else {
                con.getCn().rollback();
                res.setCodigo(resultado.getCodigo());
                res.setMensaje(resultado.getMensaje());
            }

            if (res.getCodigo().equals("0")) {
                return Response.status(200).entity(res).build();
            } else if (res.getCodigo().equals("1")) {
                return Response.status(401).entity(res).build();
            } else {
                return Response.status(409).entity(res).build();
            }
        } catch (UnsupportedEncodingException e1) {
            res.setCodigo("1");
            res.setMensaje(e1.getMessage());
            return Response.status(401).entity(res).build();
        } catch (Exception e2) {
            res.setCodigo("1");
            res.setMensaje(e2.getMessage());
            return Response.status(401).entity(res).build();
        } finally {
            con.cierraConexion();
        }
    }

    @GET
    @Path("movimiento/{documento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaMovimiento(@PathParam("documento")
        String documento) {
        Consultas consulta = new Consultas();
        Conexion con = new Conexion();
        Consulta resp = new Consulta();
        try {
            // Abrir coneccion
            con.abreConexion();
            // Persistir Persona
            resp = consulta.listaMovimiento(documento, con);
            return Response.status(200).entity(resp).build();
        } catch (UnsupportedEncodingException e1) {
            return Response.status(401).entity(resp).build();
        } catch (Exception e2) {
            return Response.status(401).entity(resp).build();
        } finally {
            con.cierraConexion();
        }
    }
}


