package sgc.bean;

import java.util.ArrayList;
import java.util.List;

public class Consulta {
    private Persona persona = new Persona();
    private List<Movimiento> movimiento = new ArrayList<Movimiento>();
    private List<Saldo> saldo = new ArrayList<Saldo>();

    public Consulta() {
        super();
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setMovimiento(List<Movimiento> movimiento) {
        this.movimiento = movimiento;
    }

    public List<Movimiento> getMovimiento() {
        return movimiento;
    }

    public void setSaldo(List<Saldo> saldo) {
        this.saldo = saldo;
    }

    public List<Saldo> getSaldo() {
        return saldo;
    }
}
