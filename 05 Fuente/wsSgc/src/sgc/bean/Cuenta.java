package sgc.bean;

public class Cuenta {
    private Persona persona = new Persona();
    private String cueNumero;
    private TipoCuenta tipoCuenta = new TipoCuenta();
    private String cueEstado = "ACTIVE";

    public Cuenta() {
        super();
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setCueNumero(String cueNumero) {
        this.cueNumero = cueNumero;
    }

    public String getCueNumero() {
        return cueNumero;
    }

    public void setTipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public TipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public void setCueEstado(String cueEstado) {
        this.cueEstado = cueEstado;
    }

    public String getCueEstado() {
        return cueEstado;
    }
}
