package sgc.bean;

public class Movimiento {
    private String movId;
    private String perDocumentoNro;
    private String cueNumero;
    private String tipNombre;
    private String tipMoneda;
    private double movMonto;
    private String movTipo;
    private String cueEstado;
    private String movUsuario;
    private String movFecha;

    public Movimiento() {
        super();
    }

    public void setMovId(String movId) {
        this.movId = movId;
    }

    public String getMovId() {
        return movId;
    }

    public void setPerDocumentoNro(String perDocumentoNro) {
        this.perDocumentoNro = perDocumentoNro;
    }

    public String getPerDocumentoNro() {
        return perDocumentoNro;
    }

    public void setCueNumero(String cueNumero) {
        this.cueNumero = cueNumero;
    }

    public String getCueNumero() {
        return cueNumero;
    }

    public void setTipNombre(String tipNombre) {
        this.tipNombre = tipNombre;
    }

    public String getTipNombre() {
        return tipNombre;
    }

    public void setTipMoneda(String tipMoneda) {
        this.tipMoneda = tipMoneda;
    }

    public String getTipMoneda() {
        return tipMoneda;
    }

    public void setMovMonto(double movMonto) {
        this.movMonto = movMonto;
    }

    public double getMovMonto() {
        return movMonto;
    }

    public void setMovTipo(String movTipo) {
        this.movTipo = movTipo;
    }

    public String getMovTipo() {
        return movTipo;
    }

    public void setCueEstado(String cueEstado) {
        this.cueEstado = cueEstado;
    }

    public String getCueEstado() {
        return cueEstado;
    }

    public void setMovUsuario(String movUsuario) {
        this.movUsuario = movUsuario;
    }

    public String getMovUsuario() {
        return movUsuario;
    }

    public void setMovFecha(String movFecha) {
        this.movFecha = movFecha;
    }

    public String getMovFecha() {
        return movFecha;
    }
}
