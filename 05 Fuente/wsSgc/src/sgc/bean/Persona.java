package sgc.bean;

public class Persona {
    private String perDocumentoNro;
    private String perDocumentoExt;
    private String perApellidoUno;
    private String perApellidoDos;
    private String perNombres;
    private String perFechaNacimiento;

    public void setPerDocumentoNro(String perDocumentoNro) {
        this.perDocumentoNro = perDocumentoNro;
    }

    public String getPerDocumentoNro() {
        return perDocumentoNro;
    }

    public void setPerDocumentoExt(String perDocumentoExt) {
        this.perDocumentoExt = perDocumentoExt;
    }

    public String getPerDocumentoExt() {
        return perDocumentoExt;
    }

    public void setPerApellidoUno(String perApellidoUno) {
        this.perApellidoUno = perApellidoUno;
    }

    public String getPerApellidoUno() {
        return perApellidoUno;
    }

    public void setPerApellidoDos(String perApellidoDos) {
        this.perApellidoDos = perApellidoDos;
    }

    public String getPerApellidoDos() {
        return perApellidoDos;
    }

    public void setPerNombres(String perNombres) {
        this.perNombres = perNombres;
    }

    public String getPerNombres() {
        return perNombres;
    }

    public void setPerFechaNacimiento(String perFechaNacimiento) {
        this.perFechaNacimiento = perFechaNacimiento;
    }

    public String getPerFechaNacimiento() {
        return perFechaNacimiento;
    }
}
