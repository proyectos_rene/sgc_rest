package sgc.bean;

public class Saldo {
    private String cueNumero;
    private double saldo;

    public Saldo() {
        super();
    }

    public void setCueNumero(String cueNumero) {
        this.cueNumero = cueNumero;
    }

    public String getCueNumero() {
        return cueNumero;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }
}
