package sgc.bean;

public class TipoCuenta {
    private String tipId;
    private String tipNombre;    

    public void setTipId(String tipId) {
        this.tipId = tipId;
    }

    public String getTipId() {
        return tipId;
    }

    public void setTipNombre(String tipNombre) {
        this.tipNombre = tipNombre;
    }

    public String getTipNombre() {
        return tipNombre;
    }
}
