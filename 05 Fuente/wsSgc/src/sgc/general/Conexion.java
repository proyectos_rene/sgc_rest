package sgc.general;

import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.DriverManager;

import java.util.ArrayList;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

public class Conexion {
    private Connection cn = null;

    public void abreConexion() throws Exception {
        try {
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource)ic.lookup("jdbc/ws_sgc");
            cn = (Connection)ds.getConnection();
            cn.setAutoCommit(false);
            /*DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            cn = DriverManager.getConnection("jdbc:oracle:thin:@0.0.0.0:1521:bds", "sgc", "sgc");
            cn.setAutoCommit(false);*/
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception("Se produjo un error al abrir la Conexion a la BD.");
        }
    }

    public void cierraConexion() {
        try {
            if (cn != null) {
                cn.close();
            }
            cn = null;
        } catch (Exception e) {
            ;
        }
    }

    public void setCn(Connection cn) {
        this.cn = cn;
    }

    public Connection getCn() {
        return cn;
    }

}
