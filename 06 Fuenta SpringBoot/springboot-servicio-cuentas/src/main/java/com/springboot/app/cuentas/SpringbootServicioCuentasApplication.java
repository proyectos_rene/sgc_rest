package com.springboot.app.cuentas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServicioCuentasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioCuentasApplication.class, args);
	}

}
