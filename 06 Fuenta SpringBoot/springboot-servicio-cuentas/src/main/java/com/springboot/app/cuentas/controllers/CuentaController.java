package com.springboot.app.cuentas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.cuentas.exception.BadException;
import com.springboot.app.cuentas.exception.ConflictException;
import com.springboot.app.cuentas.exception.UnauthorizedException;
import com.springboot.app.cuentas.models.entity.Cuenta;
import com.springboot.app.cuentas.models.entity.Respuesta;
import com.springboot.app.cuentas.models.entity.Usuario;
import com.springboot.app.cuentas.models.service.ICuentaService;
import com.springboot.app.cuentas.models.service.ITipoCuentaService;
import com.springboot.app.cuentas.models.service.IUsuarioService;

@RestController
@RequestMapping("/cuenta")
public class CuentaController {
	@Autowired
	private ICuentaService cuentaService;
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	ITipoCuentaService tipoCuentaService;

	@GetMapping("/listar")
	public List<Cuenta> listar() {
		return cuentaService.findAll();
	}

	@GetMapping("/ver/{id}")
	public Cuenta cuenta(@PathVariable Long id) {
		return cuentaService.findBydId(id);
	}

	@PostMapping()
	public ResponseEntity<?> saveCuenta(@RequestBody Cuenta cuenta)
			throws BadException, UnauthorizedException, ConflictException {

		Respuesta respuesta = null;
		try {
			if (usuarioService.findById(cuenta.getUsuario().getDocumento()) == null) {
				respuesta = new Respuesta("1",
						"No existe el Usuario con el documento: " + cuenta.getUsuario().getDocumento());
				return new ResponseEntity<Respuesta>(respuesta, HttpStatus.CONFLICT);
			}
			if (tipoCuentaService.finById(cuenta.getTipoCuenta().getIdTipoCuenta()) == null) {
				respuesta = new Respuesta("1",
						"No existe el Tipo de Cuenta: " + cuenta.getTipoCuenta().getIdTipoCuenta());
				return new ResponseEntity<Respuesta>(respuesta, HttpStatus.CONFLICT);
			}
			Cuenta resp = cuentaService.saveCuenta(cuenta);
			respuesta = new Respuesta("0", "Se grabó correctamente la Cuenta");
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.BAD_REQUEST);
		}
	}
}
