package com.springboot.app.cuentas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.cuentas.exception.BadException;
import com.springboot.app.cuentas.exception.ConflictException;
import com.springboot.app.cuentas.exception.UnauthorizedException;
import com.springboot.app.cuentas.models.entity.Cuenta;
import com.springboot.app.cuentas.models.entity.Movimiento;
import com.springboot.app.cuentas.models.entity.Respuesta;
import com.springboot.app.cuentas.models.service.ICuentaService;
import com.springboot.app.cuentas.models.service.IMovimientoService;
import com.springboot.app.cuentas.models.service.IUsuarioService;

@RestController
@RequestMapping("/movimiento")
public class MovimientoController {
	@Autowired
	private IMovimientoService movimientoService;
	@Autowired
	private IUsuarioService usuarioService;

	@GetMapping("/listar")
	public List<Movimiento> listar() {
		return movimientoService.findAll();
	}

	@GetMapping("/ver/{id}")
	public Movimiento movimiento(@PathVariable Long id) {
		return movimientoService.findBydId(id);
	}

	@PostMapping()
	public ResponseEntity<?> saveMovimiento(@RequestBody Movimiento movimiento)
			throws BadException, UnauthorizedException, ConflictException {
		Respuesta respuesta = null;
		try {
			if (usuarioService.findById(movimiento.getUsuario().getDocumento()) == null) {
				respuesta = new Respuesta("1",
						"No existe el Usuario con el documento: " + movimiento.getUsuario().getDocumento());
				return new ResponseEntity<Respuesta>(respuesta, HttpStatus.CONFLICT);
			}
			respuesta = movimientoService.saveMovimiento(movimiento);
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.BAD_REQUEST);
		}
	}
}
