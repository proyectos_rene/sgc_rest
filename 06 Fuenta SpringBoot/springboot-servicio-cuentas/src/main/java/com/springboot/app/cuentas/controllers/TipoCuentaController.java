package com.springboot.app.cuentas.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.springboot.app.cuentas.models.entity.TipoCuenta;
import com.springboot.app.cuentas.models.service.ITipoCuentaService;

@RestController
@RequestMapping("/tipo_cuenta")
public class TipoCuentaController {
	@Autowired
	ITipoCuentaService tipoCuentaService;

	@GetMapping("/listar")
	public List<TipoCuenta> listar() {
		return tipoCuentaService.finAll();
	}

	@GetMapping("/ver/{id}")
	public TipoCuenta tipoCuenta(@PathVariable Long id) {
		return tipoCuentaService.finById(id);
	}
}
