package com.springboot.app.cuentas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.cuentas.models.entity.Respuesta;
import com.springboot.app.cuentas.models.entity.Usuario;
import com.springboot.app.cuentas.models.service.IUsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	@Autowired
	private IUsuarioService usuarioService;

	@GetMapping("/listar")
	public List<Usuario> listar() {
		return usuarioService.findAll();
	}

	@GetMapping("/ver/{id}")
	public Usuario usuario(@PathVariable String id) {
		return usuarioService.findById(id);
	}

	@PostMapping()
	public ResponseEntity<?> saveUsuario(@RequestBody Usuario usuario) {
		Respuesta respuesta = null;
		try {
			Usuario usu = usuarioService.saveUsuario(usuario);			
			respuesta = new Respuesta("1", "Se creo correctamente el Usuario");
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Respuesta>(respuesta, HttpStatus.BAD_REQUEST);
		}
	}
}
