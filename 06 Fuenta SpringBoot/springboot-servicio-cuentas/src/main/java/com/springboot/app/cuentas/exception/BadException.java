package com.springboot.app.cuentas.exception;

public class BadException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadException (String message) {
		super (message);
	}
}