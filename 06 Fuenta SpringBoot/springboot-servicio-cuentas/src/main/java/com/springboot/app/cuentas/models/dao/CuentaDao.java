package com.springboot.app.cuentas.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.springboot.app.cuentas.models.entity.Cuenta;

public interface CuentaDao extends JpaRepository<Cuenta, Long> {	
}
