package com.springboot.app.cuentas.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.springboot.app.cuentas.models.entity.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, String>{

}
