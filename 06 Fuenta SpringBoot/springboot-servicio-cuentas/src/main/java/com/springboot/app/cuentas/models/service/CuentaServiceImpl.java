package com.springboot.app.cuentas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.app.cuentas.models.dao.CuentaDao;
import com.springboot.app.cuentas.models.dao.UsuarioDao;
import com.springboot.app.cuentas.models.entity.Cuenta;

@Service
public class CuentaServiceImpl implements ICuentaService {
	@Autowired
	CuentaDao cuentaDao;	
	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	public List<Cuenta> findAll() {		
		return (List<Cuenta>)cuentaDao.findAll();
	}

	@Override
	public Cuenta findBydId(Long id) {
		return cuentaDao.findById(id).orElse(null);
	}

	@Override
	public Cuenta saveCuenta(Cuenta cuenta) {		
		return cuentaDao.save(cuenta);
	}

}
