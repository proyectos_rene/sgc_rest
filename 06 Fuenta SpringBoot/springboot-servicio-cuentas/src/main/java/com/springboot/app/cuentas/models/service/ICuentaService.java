package com.springboot.app.cuentas.models.service;

import java.util.List;

import com.springboot.app.cuentas.models.entity.Cuenta;

public interface ICuentaService {
	public List<Cuenta> findAll();
	public Cuenta findBydId(Long id);
	public Cuenta saveCuenta(Cuenta cuenta);
}
