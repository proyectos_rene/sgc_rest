package com.springboot.app.cuentas.models.service;

import java.util.List;

import com.springboot.app.cuentas.models.entity.Movimiento;
import com.springboot.app.cuentas.models.entity.Respuesta;

public interface IMovimientoService {
	public List<Movimiento> findAll();

	public Movimiento findBydId(Long id);

	public Respuesta saveMovimiento(Movimiento cuenta);
}
