package com.springboot.app.cuentas.models.service;

import java.util.List;

import com.springboot.app.cuentas.models.entity.TipoCuenta;

public interface ITipoCuentaService {
	public List<TipoCuenta> finAll();

	public TipoCuenta finById(Long id);
}
