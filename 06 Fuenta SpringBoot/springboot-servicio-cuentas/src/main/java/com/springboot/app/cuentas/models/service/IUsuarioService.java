package com.springboot.app.cuentas.models.service;

import java.util.List;

import com.springboot.app.cuentas.models.entity.Usuario;

public interface IUsuarioService {
	public List<Usuario> findAll();
	public Usuario findById(String documento);
	public Usuario saveUsuario(Usuario usuario);	
}