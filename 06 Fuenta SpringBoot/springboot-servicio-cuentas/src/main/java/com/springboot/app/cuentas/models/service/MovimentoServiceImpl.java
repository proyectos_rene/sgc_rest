package com.springboot.app.cuentas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.app.cuentas.models.dao.CuentaDao;
import com.springboot.app.cuentas.models.dao.MovimientoDao;
import com.springboot.app.cuentas.models.dao.UsuarioDao;
import com.springboot.app.cuentas.models.entity.Cuenta;
import com.springboot.app.cuentas.models.entity.Movimiento;
import com.springboot.app.cuentas.models.entity.Respuesta;

@Service
public class MovimentoServiceImpl implements IMovimientoService {
	@Autowired
	MovimientoDao movimientoDao;
	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private ICuentaService cuentaService;

	@Override
	public List<Movimiento> findAll() {
		return (List<Movimiento>) movimientoDao.findAll();
	}

	@Override
	public Movimiento findBydId(Long id) {
		return movimientoDao.findById(id).orElse(null);
	}

	@Override
	public Respuesta saveMovimiento(Movimiento movimiento) {
		Cuenta cuenta = cuentaService.findBydId(movimiento.getCuenta().getIdCuenta());

		Respuesta respuesta = null;
		if ("SALIDA".equals(movimiento.getTipo())) {
			if (cuenta.getEstado().equals("HOLD")) {
				respuesta = new Respuesta("1", "No puede realizar retiros de su cuenta en estado HOLD");
			} else {
				if (cuenta.getSaldo() < movimiento.getMonto()
						&& cuentaService.findBydId(movimiento.getCuenta().getIdCuenta()).getEstado().equals("ACTIVE")) {
					movimientoDao.save(movimiento);
					cuenta.setEstado("HOLD");
					cuenta.setSaldo(cuenta.getSaldo()-movimiento.getMonto());
					cuentaService.saveCuenta(cuenta);
					respuesta = new Respuesta("0", "Se grabó correctamente el Movimiento");
				}
			}
		} else if ("INGRESO".equals(movimiento.getTipo())) {
			if (movimiento.getMonto() > cuenta.getSaldo() && cuenta.getEstado().equals("HOLD")) {				
				cuenta.setEstado("ACTIVE");
				movimientoDao.save(movimiento);
				cuenta.setSaldo(cuenta.getSaldo()+movimiento.getMonto());
				cuentaService.saveCuenta(cuenta);
				respuesta = new Respuesta("0", "Se grabó correctamente el Movimiento");
			}else {				
				movimientoDao.save(movimiento);
				cuenta.setSaldo(cuenta.getSaldo()+movimiento.getMonto());
				cuentaService.saveCuenta(cuenta);
				respuesta = new Respuesta("0", "Se grabó correctamente el Movimiento");
			}
		} else {
			respuesta = new Respuesta("1", "Error de movimiento, no corresponde a INGRESO o SALIDA");
		}
		return respuesta;
	}
}
