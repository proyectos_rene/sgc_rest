package com.springboot.app.cuentas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.cuentas.models.dao.TipoCuentaDao;
import com.springboot.app.cuentas.models.entity.TipoCuenta;

@Service
public class TipoCuentaServiceImpl implements ITipoCuentaService {
	@Autowired
	private TipoCuentaDao tipoCuentaDao; 

	@Override
	@Transactional(readOnly = true)
	public List<TipoCuenta> finAll() {
		return (List<TipoCuenta>)tipoCuentaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoCuenta finById(Long id) {
		return tipoCuentaDao.findById(id).orElse(null); 
	}

}
