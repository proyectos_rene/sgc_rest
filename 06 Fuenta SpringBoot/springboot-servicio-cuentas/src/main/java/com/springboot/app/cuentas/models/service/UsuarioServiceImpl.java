package com.springboot.app.cuentas.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.cuentas.models.dao.UsuarioDao;
import com.springboot.app.cuentas.models.entity.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(String documento) {
		return usuarioDao.findById(documento).orElse(null);
	}

	@Override
	public Usuario saveUsuario(Usuario usuario) {		
		return usuarioDao.save(usuario);		
	}

}
