INSERT INTO usuarios (documento, extension, apellido_uno, apellido_dos, nombres, fecha_nacimiento) VALUES('4839931','LP','GONZALES','TORREZ','MARIA',NOW());
INSERT INTO usuarios (documento, extension, apellido_uno, apellido_dos, nombres, fecha_nacimiento) VALUES('123456','LP','FLORES','SEGALES','JOSE',NOW());
INSERT INTO tipo_cuenta (descripcion, moneda) VALUES('CAJA DE AHORRO','BOB');
INSERT INTO tipo_cuenta (descripcion, moneda) VALUES('DEPOSITO A PLAZO FIJO','BOB');
INSERT INTO tipo_cuenta (descripcion, moneda) VALUES('CUENTA CORRIENTE','BOB');
INSERT INTO cuenta (documento, id_tipo_cuenta, nro_Cuenta, estado, saldo) VALUES('4839931','1', '111111111', 'ACTIVE', 1000);
INSERT INTO movimiento (documento, id_cuenta, monto, tipo) VALUES('4839931',1, 1000, 'INGRESO');
